
import com.custanalytics.service.EmailService
import com.custanalytics.service.impl.EmailServiceSG
import com.typesafe.config.ConfigFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FlatSpec, _}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class EmailServiceTest extends FlatSpec with Matchers with ScalaFutures {
  it should " return true after successfully sending an email" in {
    val emailService: EmailService = new EmailServiceSG

    var conf = ConfigFactory.load
    val authKey = "Bearer " + conf.getString("apiKey")
    val sender = conf.getString("sender")
    val endPoint = conf.getString("endPoint")

    val emailStatus: Future[Boolean] = emailService.sendEmail("dmuruli@gmail.com", "Test Email Notification- Scala Dispath Asynch", sender, endPoint, authKey)
    Await.ready(emailStatus, 5.second)

    whenReady(emailStatus) { result =>
      result should equal(true)
    }

  }
}
