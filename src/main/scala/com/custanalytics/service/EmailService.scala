package com.custanalytics.service

import scala.concurrent.Future

trait EmailService {

  def sendEmail(recipientAddresss: String, subject: String, senderAddress: String, endPoint: String, authKey: String): Future[Boolean]


}
