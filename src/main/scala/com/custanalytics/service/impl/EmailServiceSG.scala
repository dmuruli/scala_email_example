package com.custanalytics.service.impl

import com.custanalytics.service.EmailService
import dispatch.Defaults._
import dispatch._

import scala.Predef._
import scala.collection.immutable.Map
import scala.concurrent.Future

class EmailServiceSG extends EmailService {

  def sendEmail(recipientAddresss: String, subject: String, senderAddress: String, endPoint: String, authKey: String): Future[Boolean]
  = {

    val headers = Map("Authorization" -> authKey)

    val params = Map(
      "from" -> senderAddress,
      "to" -> recipientAddresss,
      "subject" -> subject,
      "html" -> "Unit Test Html Email-Scala Dispatch"
    )

    val req = url(endPoint).POST <:< headers <<? params
    val response: Future[String] = Http(req OK as.String)
    val responseStatus: Future[Boolean] = response.map(response =>
      response.contains("success")
    )
    responseStatus
  }

}
